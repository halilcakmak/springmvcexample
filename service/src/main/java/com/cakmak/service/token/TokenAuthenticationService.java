package com.cakmak.service.token;


import com.cakmak.model.user.User;
import org.springframework.security.core.Authentication;

public interface TokenAuthenticationService {
    String getUsernameFromToken(String token);

    String createToken(User user);

    User getUserFromToken(String token);

    String generateJwtToken(Authentication authentication);

    boolean validateJwtToken(String authToken);
}
