package com.cakmak.service.user;

import org.springframework.security.core.userdetails.UserDetailsService;
import com.cakmak.model.user.User;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User register(User user);

    User findByUsername(String username);

}
