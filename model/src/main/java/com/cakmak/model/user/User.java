package com.cakmak.model.user;

import com.google.common.collect.Sets;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "USERS", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@Builder
@EntityListeners(AuditingEntityListener.class)
public class User implements UserDetails {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotBlank
    @Size(min=3, max = 50)
    private String name;

    @NotBlank
    @Size(min=3, max = 50)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @CreatedDate
    private Date registered;

    private Boolean enabled;

    @NotBlank
    @Size(min=6, max = 100)
    private String password;

    private String token;

    @ElementCollection(targetClass = RoleName.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "USER_ROLES")
    @Column(name = "user_roles")
    Set<RoleName> roleNames = new HashSet<>(Sets.newHashSet(RoleName.ROLE_USER));

    public boolean hasRoles(RoleName... roles) {

        for (RoleName role : roles) {
            if (!roleNames.contains(role)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        if (hasRoles(RoleName.ROLE_USER)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(RoleName.ROLE_USER.name()));
        }

        if (hasRoles(RoleName.ROLE_TEST)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(RoleName.ROLE_TEST.name()));
        }

        if (hasRoles(RoleName.ROLE_ADMIN)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.name()));
        }
        return grantedAuthorities;
    }

    public User(String name,String username,String email,String password){
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}

