package com.cakmak.model.user;

import lombok.Getter;

public enum RoleName {
    ROLE_USER,ROLE_TEST, ROLE_ADMIN;
}
