package com.cakmak.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@ComponentScan(basePackages = "com.cakmak.service", includeFilters = @ComponentScan.Filter(value = Service.class, type = FilterType.ANNOTATION))
public class ServiceConfig {
}
