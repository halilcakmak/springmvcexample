package com.cakmak.rest;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;

@Controller
public class HomeController {

//    @NonNull
//    private final TagService tagService;
//
//    @Autowired
//    public HomeController(TagService tagService) {
//        this.tagService = tagService;
//    }

    @RequestMapping("/")
    public String home() {
        return "redirect:/swagger-ui.html";
    }

}
