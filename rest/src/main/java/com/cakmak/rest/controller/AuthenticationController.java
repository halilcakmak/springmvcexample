package com.cakmak.rest.controller;

import com.cakmak.rest.controller.form.AuthForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.cakmak.model.user.User;
import com.cakmak.service.token.TokenAuthenticationService;
import com.cakmak.service.user.UserService;

@Slf4j
@RestController
public class AuthenticationController {

    private final TokenAuthenticationService tokenAuthenticationService;
    private final UserService userService;

    @Autowired
    public AuthenticationController(TokenAuthenticationService tokenAuthenticationService, UserService userService) {
        this.tokenAuthenticationService = tokenAuthenticationService;
        this.userService = userService;
    }

    @PostMapping("/auth/sign")
    @ResponseStatus(HttpStatus.CREATED)
    public User authorizeUser(@RequestBody AuthForm authForm) throws AuthenticationException {
        String email = authForm.getEmail();
        User user = userService.findByEmail(email);
        if (user == null) {
            user = fetchDataFromAuthForm(authForm);
            user = userService.register(user);
        }
        final String token = tokenAuthenticationService.createToken(user);
        return user;
    }

    private User fetchDataFromAuthForm(AuthForm authForm) {
        return User.builder()
                .email(authForm.getEmail())
                .name(authForm.getFirstName())
                .username(authForm.getUsername())
                .password(authForm.getPassword())
                .enabled(Boolean.TRUE)
                .build();
    }

}
